# Einrichtung Remote Arbeitsplatz mit Xubuntu

## Distribution
Verwendet werden sollte ein Linux. Vorzugsweise xubuntu Vers.>=20.04

## VPN
Zugang wird über openvpn realisiert. Installiert werden muss:

```bash
sudo apt-get install openvpn network-manager-openvpn-gnome
```

## Remotedesktop
Die Verbindung zum Host wird über das rdp Protokoll realisiert. Software die dafür bentigt werden muss:

```bash
sudo apt-get install remmina
```

## Konfiguration

### vpn
Damit ubuntu die ovpn Datei importieren kann, muss folgende Zeile auskommentiert werden:

```
#route remote_host 255.255.255.255 net_gateway
```

### Surfen bei bestehender VPN Verbindung

Um bei bestehender VPN Verbindung im Internet surfen zu können, muss unter:

```
vpn-verbindung->vpn konfigurieren->vpn verbindung (owa)->ipv4-einstellungen->routen->diese verbindung nur für resourcen dieses netztwerks verwenden
```

geklickt werden.


### Remote Desktop via Remmina und freerdp
Damit Remmina Schriften korrekt darstellt muss unter

"Erweitert" > "Qualität" > "Beste (langsam)"

ausgewählt werden.


